import {kebab} from 'case';

// Subentity
export const listSubentityPath = (subentityName: string): string =>
  `/entity/:id/subentities/${kebab(subentityName)}/list`;
export const createSubentityPath = (subentityName: string): string =>
  `/entity/:id/subentities/${kebab(subentityName)}/entity`;
export const updateSubentityPath = (subentityName: string): string =>
  `/entity/:id/subentities/${kebab(subentityName)}/entity/:subid`;
export const deleteSubentityPath = updateSubentityPath;

// GeneralSubentity
export const createGeneralSubentityPath = (subentityName: string): string =>
  `/subentities/${kebab(subentityName)}/entity`;
export const updateGeneralSubentityPath = (subentityName: string): string =>
  `/subentities/${kebab(subentityName)}/entity/:id`;
export const byIdGeneralSubentityPath = (subentityName: string): string =>
  `/subentities/${kebab(subentityName)}/entity/:id`;
export const listGeneralSubentityPath = (subentityName: string): string =>
  `/subentities/${kebab(subentityName)}/list`;
export const listRequestGeneralSubentityPath = (
  subentityName: string,
): string => `/subentities/${kebab(subentityName)}/list`;

export const generalSubentityGeneralMethodPath = (
  subentityName: string,
  methodName: string,
): string =>
  `/subentities/${kebab(subentityName)}/methods/${kebab(methodName)}`;

export const generalSubentityEntityMethodPath = (
  subentityName: string,
  methodName: string,
): string =>
  `/subentities/${kebab(subentityName)}/entity/:id/methods/${kebab(
    methodName,
  )}`;

// Basic
export const listPath = (): string => '/list';
export const createPath = (): string => '/entity/';
export const getPath = (): string => '/entity/:id/';
export const updatePath = getPath;
export const deletePath = getPath;
export const generalMethodPath = (methodName: string): string =>
  `/methods/${kebab(methodName)}`;
export const generalGetMethodPath = (methodName: string): string =>
  `/methods/${kebab(methodName)}`;
export const entityMethodPath = (methodName: string): string =>
  `/entity/:id/methods/${kebab(methodName)}`;

export const generalSubentityReprGetPath = (subentityName: string): string =>
  `/${kebab(subentityName)}/entity/:id`;
export const generalSubentityReprListPath = (subentityName: string): string =>
  `/${kebab(subentityName)}/list`;
