import {Router} from 'express';

export abstract class BaseController {
    public abstract getRouter(): Router;

    protected error(error: any) {
      return {
        error,
        status: 'error',
      };
    }

    protected sucess(data: any) {
      return {
        data,
        status: 'success',
      };
    }
}
