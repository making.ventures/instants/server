import express, {Request, Response} from 'express';
import {BaseController} from '../BaseController';
import {IUIStrService, IBaseEntity, IListRequest, RequiredId} from '@instants/core';
import {uiCreate, uiGetStr, uiList, uiUpdate} from '../methods';
import {BaseReprStrController} from '../BaseReprStrController';

export abstract class BaseUIStrController<
  TGet extends IBaseEntity<string>,
  TUpdate,
  TCreate,
  TForList,
  Repo extends IUIStrService<TGet, TUpdate, TCreate, TForList, IListRequest>
  > extends BaseController {
  protected reprController: BaseReprStrController<TGet, Repo>;

  protected service: Repo;

  public constructor(service: Repo) {
    super();
    this.service = service;
    this.reprController = new BaseReprStrController<TGet, Repo>(this.service);
  }

  public getRouter() {
    const router = express.Router();
    const uiRouter = this.getUIRouter();
    router.use('/ui', uiRouter);
    const reprRouter = this.reprController.getReprRouter();
    router.use('/repr', reprRouter);

    return router;
  }

  public getUIRouter() {
    const uiRouter = express.Router();
    uiRouter.get('/list/', this.uiList.bind(this));
    uiRouter.post('/entity/', this.uiCreate.bind(this));
    uiRouter.get('/entity/:id', this.uiGet.bind(this));
    uiRouter.put('/entity/:id', this.uiUpdate.bind(this));

    return uiRouter;
  }

  public async uiGet(req: Request, res: Response) {
    await uiGetStr<TGet>(req, res, this.service.uiGet.bind(this.service));
  }

  public async uiCreate(req: Request, res: Response) {
    await uiCreate<TCreate, TGet>(
      req,
      res,
      this.service.uiCreate.bind(this.service),
    );
  }

  public async uiList(req: Request, res: Response) {
    await uiList<TForList, IListRequest>(
      req,
      res,
      this.service.uiList.bind(this.service),
    );
  }

  public async uiUpdate(req: Request, res: Response) {
    await uiUpdate<RequiredId<TUpdate>, TGet>(
      req,
      res,
      this.service.uiUpdate.bind(this.service),
    );
  }
}
