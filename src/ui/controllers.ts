import {Request, Response, Router} from 'express';
import {
  TGet,
  TCreate,
  TList,
  TUpdate,
} from '@instants/core';
import {
  uiUpdate,
  uiList,
  uiCreate,
  uiGet,
} from '../methods';
import {
  updatePath,
  createPath,
  listPath,
  getPath,
} from '../paths';

export const addUiGet = <T>(service: { uiGet: TGet<T> }) => ({
  uiGet: async (req: Request, res: Response) => {
    await uiGet<T>(req, res, service.uiGet.bind(service));
  },
});

export const addUiCreate = <TUICreate, TUIGet>(service: {
  uiCreate: TCreate<TUICreate, TUIGet>;
}) => ({
    uiCreate: async (req: Request, res: Response) => {
      await uiCreate<TUICreate, TUIGet>(req, res, service.uiCreate.bind(service));
    },
  });

export const addUiList = <TUIForListRequest, TUIForList>(service: {
  uiList: TList<TUIForListRequest, TUIForList>;
}) => ({
    uiList: async (req: Request, res: Response) => {
      await uiList<TUIForListRequest, TUIForList>(
        req,
        res,
        service.uiList.bind(service) as any,
      );
    },
  });

export const addUiUpdate = <TUIUpdate, TUIGet>(service: {
  uiUpdate: TUpdate<TUIUpdate, TUIGet>;
}) => ({
    uiUpdate: async (req: Request, res: Response) => {
      await uiUpdate<TUIUpdate, TUIGet>(req, res, service.uiUpdate.bind(service));
    },
  });

export const addUiListPath = (
  router: Router,
  service: {
    uiList: (req: Request, res: Response) => Promise<void>;
  },
) => router.get(listPath(), service.uiList.bind(service));

export const addUiCreatePath = (
  router: Router,
  service: {
    uiCreate: (req: Request, res: Response) => Promise<void>;
  },
) => router.post(createPath(), service.uiCreate.bind(service));

export const addUiGetPath = (
  router: Router,
  service: {
    uiGet: (req: Request, res: Response) => Promise<void>;
  },
) => router.get(getPath(), service.uiGet.bind(service));

export const addUiUpdatePath = (
  router: Router,
  service: {
    uiUpdate: (req: Request, res: Response) => Promise<void>;
  },
) => router.put(updatePath(), service.uiUpdate.bind(service));

// export class UILRController<
//   Model extends IBaseEntity<IdType<Model>>,
//   TForList,
//   TForListRequest,
//   Repo extends IUILRService<Model, TForList, TForListRequest>
// > extends BaseServiceController<Model, Repo> implements IUILRController {
//   public getUIRouter() {
//     const router = Router();
//     router.get(listPath(), this.uiList.bind(this));

//     return router;
//   }

//   uiList = addUiList(this.service).uiList;
// }

// export class UILController<
//   Model extends IBaseEntity<IdType<Model>>,
//   TForList,
//   Repo extends IUILService<Model, TForList, IListRequest>
// > extends BaseServiceController<Model, Repo> implements IUILController {
//   public getUIRouter() {
//     const router = Router();
//     router.get(listPath(), this.uiList.bind(this));

//     return router;
//   }

//   uiList = addUiList(this.service).uiList;
// }

// export class UICLRController<
//   Model extends IBaseEntity<IdType<Model>>,
//   TGet,
//   TCreate,
//   TForList,
//   TForListRequest,
//   Repo extends IUICLRService<Model, TGet, TCreate, TForList, TForListRequest>
// > extends BaseServiceController<Model, Repo> implements IUICLRController {
//   public getUIRouter() {
//     const router = Router();
//     router.get(listPath(), this.uiList.bind(this));
//     router.post(createPath(), this.uiCreate.bind(this));

//     return router;
//   }

//   uiCreate = addUiCreate(this.service).uiCreate;

//   uiList = addUiList(this.service).uiList;
// }

// export class UICLUController<
//   Model extends IBaseEntity<IdType<Model>>,
//   TGet,
//   TUpdate,
//   TCreate,
//   TForList,
//   Repo extends IUICLUService<
//     Model,
//     TGet,
//     TUpdate,
//     TCreate,
//     TForList,
//     IListRequest
//   >
// > extends BaseServiceController<Model, Repo> implements IUICLUController {
//   public getUIRouter() {
//     const router = Router();
//     router.get(listPath(), this.uiList.bind(this));
//     router.post(createPath(), this.uiCreate.bind(this));
//     router.put(updatePath(), this.uiUpdate.bind(this));

//     return router;
//   }

//   uiCreate = addUiCreate(this.service).uiCreate;

//   uiList = addUiList(this.service).uiList;

//   uiUpdate = addUiUpdate(this.service).uiUpdate;
// }

// export class UILUController<
//   Model extends IBaseEntity<IdType<Model>>,
//   TGet,
//   TUpdate,
//   TForList,
//   Repo extends IUILUService<Model, TGet, TUpdate, TForList, IListRequest>
// > extends BaseServiceController<Model, Repo> implements IUILUController {
//   public getUIRouter() {
//     const router = Router();
//     router.get(listPath(), this.uiList.bind(this));
//     router.put(updatePath(), this.uiUpdate.bind(this));

//     return router;
//   }

//   uiList = addUiList(this.service).uiList;

//   uiUpdate = addUiUpdate(this.service).uiUpdate;
// }

// export class UICLLangUController<
//   Model extends IBaseEntity<IdType<Model>>,
//   TGet,
//   TUpdate,
//   TCreate,
//   TForList,
//   Repo extends IUICLLangUService<Model, TGet, TUpdate, TCreate, TForList>
// > extends BaseServiceController<Model, Repo> implements IUICLLangUController {
//   public getUIRouter() {
//     const router = Router();
//     router.get(listPath(), this.uiList.bind(this));
//     router.post(createPath(), this.uiCreate.bind(this));
//     router.put(updatePath(), this.uiUpdate.bind(this));

//     return router;
//   }

//   uiCreate = addUiCreate(this.service).uiCreate;

//   uiList = addUiList(this.service).uiList;

//   uiUpdate = addUiUpdate(this.service).uiUpdate;
// }

// export class UICLRUController<
//   Model extends IBaseEntity<IdType<Model>>,
//   TGet,
//   TUpdate,
//   TCreate,
//   TForList,
//   TForListRequest,
//   Repo extends IUICLRUService<
//     Model,
//     TGet,
//     TUpdate,
//     TCreate,
//     TForList,
//     TForListRequest
//   >
// > extends BaseServiceController<Model, Repo> implements IUICLRUController {
//   public getUIRouter() {
//     const router = Router();
//     router.get(listPath(), this.uiList.bind(this));
//     router.post(createPath(), this.uiCreate.bind(this));
//     router.put(updatePath(), this.uiUpdate.bind(this));

//     return router;
//   }

//   uiCreate = addUiCreate(this.service).uiCreate;

//   uiList = addUiList(this.service).uiList;

//   uiUpdate = addUiUpdate(this.service).uiUpdate;
// }

// export class UICIULRDController<
//   Model extends IBaseEntity<IdType<Model>>,
//   TGet extends IHaveId<IdType<Model>>,
//   TUpdate,
//   TCreate,
//   TForList,
//   TForListRequest,
//   Repo extends IUICIULRDService<
//     Model,
//     TGet,
//     TUpdate,
//     TCreate,
//     TForList,
//     TForListRequest
//   >
// > extends BaseServiceController<Model, Repo> implements IUICIULRDController {
//   public getUIRouter() {
//     const router = Router();
//     router.get(getPath(), this.uiGet.bind(this));
//     router.get(listPath(), this.uiList.bind(this));
//     router.post(createPath(), this.uiCreate.bind(this));
//     router.put(updatePath(), this.uiUpdate.bind(this));

//     return router;
//   }

//   uiGet = addUiGet(this.service).uiGet;

//   uiCreate = addUiCreate(this.service).uiCreate;

//   uiList = addUiList(this.service).uiList;

//   uiUpdate = addUiUpdate(this.service).uiUpdate;
// }

// export class UICIULLangDController<
//   Model extends IBaseEntity<IdType<Model>>,
//   TGet,
//   TUpdate,
//   TCreate,
//   TForList,
//   Repo extends IUICIULLangDService<Model, TGet, TUpdate, TCreate, TForList>
// > extends BaseServiceController<Model, Repo> implements IUICIULLangDController {
//   public getUIRouter() {
//     const router = Router();
//     router.get(listPath(), this.uiList.bind(this));
//     router.get(getPath(), this.uiGet.bind(this));
//     router.post(createPath(), this.uiCreate.bind(this));
//     router.put(updatePath(), this.uiUpdate.bind(this));

//     return router;
//   }

//   uiGet = addUiGet(this.service).uiGet;

//   uiCreate = addUiCreate(this.service).uiCreate;

//   uiList = addUiList(this.service).uiList;

//   uiUpdate = addUiUpdate(this.service).uiUpdate;
// }

// export class UICULDController<
//   Model extends IBaseEntity<IdType<Model>>,
//   TGet,
//   TUpdate,
//   TCreate,
//   TForList,
//   Repo extends IUICULDService<
//     Model,
//     TGet,
//     TUpdate,
//     TCreate,
//     TForList,
//     IListRequest
//   >
// > extends BaseServiceController<Model, Repo> implements IUICULDController {
//   public getUIRouter() {
//     const router = Router();
//     router.get(listPath(), this.uiList.bind(this));
//     router.post(createPath(), this.uiCreate.bind(this));
//     router.put(updatePath(), this.uiUpdate.bind(this));

//     return router;
//   }

//   uiCreate = addUiCreate(this.service).uiCreate;

//   uiList = addUiList(this.service).uiList;

//   uiUpdate = addUiUpdate(this.service).uiUpdate;
// }

// export class UICULRDController<
//   Model extends IBaseEntity<IdType<Model>>,
//   TGet,
//   TUpdate,
//   TCreate,
//   TForList,
//   TForListRequest,
//   Repo extends IUICULRDService<
//     Model,
//     TGet,
//     TUpdate,
//     TCreate,
//     TForList,
//     TForListRequest
//   >
// > extends BaseServiceController<Model, Repo> implements IUICULDController {
//   public getUIRouter() {
//     const router = Router();
//     router.get(listPath(), this.uiList.bind(this));
//     router.post(createPath(), this.uiCreate.bind(this));
//     router.put(updatePath(), this.uiUpdate.bind(this));

//     return router;
//   }

//   uiCreate = addUiCreate(this.service).uiCreate;

//   uiList = addUiList(this.service).uiList;

//   uiUpdate = addUiUpdate(this.service).uiUpdate;

//   // tslint:disable-next-line: max-file-line-count
// }
