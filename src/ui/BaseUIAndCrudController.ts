import express, {Request, Response} from 'express';
import {uiCreate, uiGet, uiList, uiUpdate} from '../methods';
import {IBaseEntity, IListRequest, IBaseUIService} from '@instants/core';
import {BaseServiceController} from '../BaseServiceController';

export abstract class BaseUIAndCrudController<
  Model extends IBaseEntity<number>,
  TGet,
  TUpdate,
  TCreate,
  TForList,
  Repo extends IBaseUIService<
    Model,
    TGet,
    TUpdate,
    TCreate,
    TForList,
    IListRequest
  >
  > extends BaseServiceController<Model, Repo> {
  public constructor(service: Repo) {
    super(service);
  }

  public getRouter() {
    const router = express.Router();
    const basicRouter = express.Router();
    basicRouter.get('/', this.list.bind(this));
    basicRouter.get('/:id', this.get.bind(this));
    basicRouter.post('/', this.create.bind(this));
    basicRouter.put('/:id', this.update.bind(this));
    basicRouter.delete('/:id', this.delete.bind(this));
    router.use('/basic', basicRouter);
    const uiRouter = this.getUIRouter();
    router.use('/ui', uiRouter);
    const reprRouter = this.reprController.getReprRouter();
    router.use('/repr', reprRouter);

    return router;
  }

  public getUIRouter() {
    const uiRouter = express.Router();
    uiRouter.get('/list/', this.uiList.bind(this));
    uiRouter.post('/entity/', this.uiCreate.bind(this));
    uiRouter.get('/entity/:id', this.uiGet.bind(this));
    uiRouter.put('/entity/:id', this.uiUpdate.bind(this));

    return uiRouter;
  }

  public async uiGet(req: Request, res: Response) {
    await uiGet<TGet>(req, res, this.service.uiGet.bind(this.service));
  }

  public async uiCreate(req: Request, res: Response) {
    await uiCreate<TCreate, TGet>(
      req,
      res,
      this.service.uiCreate.bind(this.service),
    );
  }

  public async uiList(req: Request, res: Response) {
    await uiList<TForList, IListRequest>(
      req,
      res,
      this.service.uiList.bind(this.service),
    );
  }

  public async uiUpdate(req: Request, res: Response) {
    await uiUpdate<TUpdate, TGet>(
      req,
      res,
      this.service.uiUpdate.bind(this.service),
    );
  }
}
