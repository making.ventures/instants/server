import express, {Request, Response} from 'express';
import {BaseController} from '../BaseController';
import {BaseReprController} from '../BaseReprController';
import {IUILangService, IBaseEntity, ILangListRequest, RequiredId} from '@instants/core';
import {uiCreate, uiGet, uiList, uiUpdate} from '../methods';

export abstract class BaseUILangController<
  TGet extends IBaseEntity<number>,
  TUpdate,
  TCreate,
  TForList,
  Repo extends IUILangService<TGet, TUpdate, TCreate, TForList>
  > extends BaseController {
  protected reprController: BaseReprController<TGet, Repo>;

  protected service: Repo;

  // Extends BaseServiceController<Model, Repo> {

  public constructor(service: Repo) {
    super();
    this.service = service;
    this.reprController = new BaseReprController<TGet, Repo>(
      this.service,
    );
  }

  public getRouter() {
    const router = express.Router();
    const uiRouter = this.getUIRouter();
    router.use('/ui', uiRouter);
    const reprRouter = this.reprController.getReprRouter();
    router.use('/repr', reprRouter);

    return router;
  }

  public getUIRouter() {
    const uiRouter = express.Router();
    uiRouter.get('/list/', this.uiList.bind(this));
    uiRouter.post('/entity/', this.uiCreate.bind(this));
    uiRouter.get('/entity/:id', this.uiGet.bind(this));
    uiRouter.put('/entity/:id', this.uiUpdate.bind(this));

    return uiRouter;
  }

  public async uiGet(req: Request, res: Response) {
    await uiGet<TGet>(
      req,
      res,
      this.service.uiGet.bind(this.service),
    );
  }

  public async uiCreate(req: Request, res: Response) {
    await uiCreate<TCreate, TGet>(
      req,
      res,
      this.service.uiCreate.bind(this.service),
    );
  }

  public async uiList(req: Request, res: Response) {
    await uiList<TForList, ILangListRequest>(
      req,
      res,
      this.service.uiList.bind(this.service),
    );
  }

  public async uiUpdate(req: Request, res: Response) {
    await uiUpdate<RequiredId<TUpdate>, TGet>(
      req,
      res,
      this.service.uiUpdate.bind(this.service),
    );
  }
}
