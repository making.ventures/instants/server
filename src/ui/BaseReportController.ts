import express, {Request, Response} from 'express';
import log from 'loglevel';
import {
  IReport,
  IReportRequest,
} from '@instants/core';
import {BaseController} from '../BaseController';

export abstract class BaseReportController<
  Element extends object
  > extends BaseController {
  protected service: IReport<Element, IReportRequest<Element>>;

  public constructor(
    service: IReport<Element, IReportRequest<Element>>,
  ) {
    super();
    this.service = service;
  }

  public async calculate(req: Request, res: Response) {
    try {
      const result = await this.service.calculate(req.body);
      res.send(this.sucess(result));
    } catch (error) {
      log.error(`BaseReportController calculate. err: ${error}`);
      res.send(this.error(error));
    }
  }

  public getRouter() {
    const router = express.Router();
    router.get('/', this.calculate.bind(this));

    return router;
  }
}
