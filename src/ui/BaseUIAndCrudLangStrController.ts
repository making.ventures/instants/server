import express, {Request, Response} from 'express';
import {uiCreate, uiList, uiUpdate, uiGetStr} from '../methods';
import {IListRequest, IBaseEntity, IBaseUIStrService} from '@instants/core';
import {BaseServiceStrController} from '../BaseServiceStrController';
import {BaseReprStrController} from '../BaseReprStrController';

export abstract class BaseUIAndCrnudLangStrController<
  Model extends IBaseEntity<string>,
  TGet,
  TUpdate,
  TCreate,
  TForList,
  Repo extends IBaseUIStrService<
    Model,
    TGet,
    TUpdate,
    TCreate,
    TForList,
    IListRequest
  >
  > extends BaseServiceStrController<Model, Repo> {
  public constructor(service: Repo) {
    super(service);

    // This.service = service;
    this.reprController = new BaseReprStrController<Model, Repo>(this.service);
  }

  public getRouter() {
    const router = express.Router();

    const basicRouter = express.Router();
    basicRouter.get('/list/', this.list.bind(this));
    basicRouter.get('/entity/:id', this.get.bind(this));
    basicRouter.post('/entity/', this.create.bind(this));
    basicRouter.put('/entity/:id', this.update.bind(this));
    basicRouter.delete('/entity/:id', this.delete.bind(this));
    router.use('/basic', basicRouter);

    const uiRouter = this.getUIRouter();
    router.use('/ui', uiRouter);
    const reprRouter = this.reprController.getReprRouter();
    router.use('/repr', reprRouter);

    return router;
  }

  public getUIRouter() {
    const uiRouter = express.Router();
    uiRouter.get('/list/', this.uiList.bind(this));
    uiRouter.post('/entity/', this.uiCreate.bind(this));
    uiRouter.get('/entity/:id', this.uiGet.bind(this));
    uiRouter.put('/entity/:id', this.uiUpdate.bind(this));

    return uiRouter;
  }

  public async uiGet(req: Request, res: Response) {
    await uiGetStr<TGet>(
      req,
      res,
      this.service.uiGet.bind(this.service),
    );
  }

  public async uiCreate(req: Request, res: Response) {
    await uiCreate<TCreate, TGet>(
      req,
      res,
      this.service.uiCreate.bind(this.service),
    );
  }

  public async uiList(req: Request, res: Response) {
    await uiList<TForList, IListRequest>(
      req,
      res,
      this.service.uiList.bind(this.service),
    );
  }

  public async uiUpdate(req: Request, res: Response) {
    await uiUpdate<TUpdate, TGet>(
      req,
      res,
      this.service.uiUpdate.bind(this.service),
    );
  }
}
