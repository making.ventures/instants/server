import express, {Request, Response} from 'express';
import log from 'loglevel';
import {
  IReportWithTotal,
  IReportRequest,
} from '@instants/core';
import {BaseController} from '../BaseController';

export abstract class BaseReportWithTotalController<
  Element extends object
  > extends BaseController {
  protected service: IReportWithTotal<Element, IReportRequest<Element>>;

  public constructor(
    service: IReportWithTotal<Element, IReportRequest<Element>>,
  ) {
    super();
    this.service = service;
  }

  public async calculate(req: Request, res: Response) {
    try {
      const result = await this.service.calculate(req.query as any);
      res.send(this.sucess(result));
    } catch (error) {
      log.error(`BaseReportWithTotalController calculate. err: ${error}`);
      res.send(this.error(error));
    }
  }

  public getRouter() {
    const router = express.Router();
    router.get('/', this.calculate.bind(this));

    return router;
  }
}
