import {Request, Response} from 'express';
import log from 'loglevel';
import {
  IBaseSort,
  IUIRepresentation,
  IBaseEntity,
  TId,
  IPageInfo,
  TListRequest,
  TDelete,
  TGet,
  TUpdate,
  TDeleteStr,
  TGetStr,
  TList,
  RequiredId,
} from '@instants/core';

export const error = (err: Error) => ({
  err: err.toString(),
  status: 'error',
});
export const sucess = (data: any, pageInfo?: IPageInfo) => ({
  data,
  pageInfo,
  status: 'success',
});

export const deleteMethod = async (
  req: Request,
  res: Response,
  method: TDelete,
) => {
  try {
    const result = await method(parseInt(req.params.id, 10));
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared deleteMethod. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const deleteMethodStr = async (
  req: Request,
  res: Response,
  method: TDeleteStr,
) => {
  try {
    const result = await method(req.params.id);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared deleteMethod. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const list = async <TForList, TForListRequest>(
  req: Request,
  res: Response,
  method: TList<TForListRequest & IBaseSort<keyof TForList>, TForList>,
) => {
  try {
    const {list, pageInfo} = await method(
      (req.query as unknown) as TForListRequest & IBaseSort<keyof TForList>,
    );
    res.send(sucess(list, pageInfo));
  } catch (error_) {
    log.error(`shared list. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const create = async <E>(
  req: Request,
  res: Response,
  method: (entity: E) => Promise<E>,
) => {
  try {
    const result = await method(req.body);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared create. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const get = async <T extends IBaseEntity<number>>(
  req: Request,
  res: Response,
  method: TGet<T>,
) => {
  try {
    const result = await method(parseInt(req.params.id, 10));

    // const result = await method(req.params.id);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared get. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const getStr = async <T extends IBaseEntity<string>>(
  req: Request,
  res: Response,
  method: TGetStr<T>,
) => {
  try {
    const result = await method(req.params.id);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared get. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const update = async <Model>(
  req: Request,
  res: Response,
  method: TUpdate<Model, Model>,
) => {
  try {
    req.body.id = req.params.id;
    const result = await method(req.body);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared update. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const reprGet = async (
  req: Request,
  res: Response,
  method: TGet<IUIRepresentation<number>>,
) => {
  try {
    const result = await method(parseInt(req.params.id, 10));
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiList reprGet. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const reprGetStr = async (
  req: Request,
  res: Response,
  method: TGetStr<IUIRepresentation<string>>,
) => {
  try {
    const result = await method(req.params.id);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiList reprGet. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const reprList = async <
  T extends TId = number,
  Model extends IBaseEntity<T> = IBaseEntity<T>
>(
  req: Request,
  res: Response,
  method: TList<
    TListRequest<Model> & IBaseSort<keyof Model>,
    IUIRepresentation<T>
  >,
) => {
  try {
    const {list, pageInfo} = await method(
      (req.query as unknown) as TListRequest<Model> & IBaseSort<keyof Model>,
    );
    res.send(sucess(list, pageInfo));
  } catch (error_) {
    log.error(`shared list. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiList = list;

// export const uiList = async <TForList, TForListRequest>(
//   req: Request,
//   res: Response,
//   method: (request?: TForListRequest) => Promise<TForList[]>,
// ) => {
//   try {
//     const result = await method(req.query);
//     res.send(sucess(result));
//   } catch (error_) {
//     log.error(`shared uiList. err: ${error_}`);
//     res.send(error(error_));
//     throw error_;
//   }
// };

// export const uiList = async <TForList, TForListRequest>(
//   req: Request,
//   res: Response,
//   method: (request?: TForListRequest) => Promise<IList<TForList>>,
// ) => {
//   try {
//     const {list, pageInfo} = await method(req.query);
//     res.send(sucess(list, pageInfo));
//   } catch (error_) {
//     log.error(`shared uiList. err: ${error_}`);
//     res.send(error(error_));
//     throw error_;
//   }
// };

export const uiCreate = async <TCreate, TGet>(
  req: Request,
  res: Response,
  method: (entity: TCreate) => Promise<TGet>,
) => {
  try {
    const result = await method(req.body);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiCreate. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiCreateSubentity = async <TCreate, TGet>(
  req: Request,
  res: Response,
  method: (id: number, entity: TCreate) => Promise<TGet>,
) => {
  try {
    const result = await method(parseInt(req.params.id, 10), req.body);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiCreateSubentity. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiCreateSubentityStr = async <TCreate, TGet>(
  req: Request,
  res: Response,
  method: (id: string, entity: TCreate) => Promise<TGet>,
) => {
  try {
    const result = await method(req.params.id, req.body);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiCreateSubentity. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiCreateGeneralSubentity = async <TCreate, TGet>(
  req: Request,
  res: Response,
  method: (entity: TCreate) => Promise<TGet>,
) => {
  await uiCreate<TCreate, TGet>(req, res, method);
};

export const uiDelete = deleteMethod;

export const uiDeleteStr = deleteMethodStr;

export const uiDeleteSubentity = async (
  req: Request,
  res: Response,
  method: (id: number, subid: string) => Promise<number>,
) => {
  try {
    const result = await method(parseInt(req.params.id, 10), req.params.subid);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiDeleteSubentity. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiDeleteSubentityStr = async (
  req: Request,
  res: Response,
  method: (id: string, subid: string) => Promise<string>,
) => {
  try {
    const result = await method(req.params.id, req.params.subid);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiDeleteSubentity. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiDeleteGeneralSubentity = async (
  req: Request,
  res: Response,
  method: TDelete,
) => {
  await uiDelete(req, res, method);
};

export const uiDeleteGeneralSubentityStr = async (
  req: Request,
  res: Response,
  method: TDeleteStr,
) => {
  await uiDeleteStr(req, res, method);
};

export const uiGet = async <TGet>(
  req: Request,
  res: Response,
  method: (id: number) => Promise<TGet | null>,
) => {
  try {
    const result = await method(parseInt(req.params.id, 10));
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiGet. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiGetStr = async <TGet>(
  req: Request,
  res: Response,
  method: (id: string) => Promise<TGet | null>,
) => {
  try {
    const result = await method(req.params.id);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiGet. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiGetLang = async <TGet>(
  req: Request,
  res: Response,
  method: (id: number, lang: string) => Promise<TGet>,
) => {
  try {
    const result = await method(parseInt(req.params.id, 10), req.params.lang);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiGet. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiGetLangStr = async <TGet>(
  req: Request,
  res: Response,
  method: (id: string, lang: string) => Promise<TGet>,
) => {
  try {
    const result = await method(req.params.id, req.params.lang);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiGet. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiGetSubentity = async <TGet>(
  req: Request,
  res: Response,
  method: (id: number, subid: number) => Promise<TGet>,
) => {
  try {
    const result = await method(
      parseInt(req.params.id, 10),
      parseInt(req.params.subid, 10),
    );
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiGetSubentity. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiGetSubentityStr = async <TGet>(
  req: Request,
  res: Response,
  method: (id: string, subid: string) => Promise<TGet>,
) => {
  try {
    const result = await method(req.params.id, req.params.subid);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiGetSubentity. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiGetGeneralSubentity = async <TGet>(
  req: Request,
  res: Response,
  method: (id: number) => Promise<TGet>,
) => {
  await uiGet<TGet>(req, res, method);
};

export const uiGetGeneralSubentityStr = async <TGet>(
  req: Request,
  res: Response,
  method: (id: string) => Promise<TGet>,
) => {
  await uiGetStr<TGet>(req, res, method);
};

export const uiUpdate = async <TUpdate, TGet>(
  req: Request,
  res: Response,
  method: (entity: RequiredId<TUpdate>) => Promise<TGet>,
) => {
  try {
    req.body.id = req.params.id;
    const result = await method(req.body);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiUpdate. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiUpdateSubentity = async <TUpdate, TGet>(
  req: Request,
  res: Response,
  method: (id: number, entity: TUpdate) => Promise<TGet>,
) => {
  try {
    req.body.id = req.params.subid;
    const result = await method(parseInt(req.params.id, 10), req.body);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiUpdateSubentity. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiUpdateSubentityStri = async <TUpdate, TGet>(
  req: Request,
  res: Response,
  method: (id: string, entity: TUpdate) => Promise<TGet>,
) => {
  try {
    req.body.id = req.params.subid;
    const result = await method(req.params.id, req.body);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiUpdateSubentity. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiUpdateGeneralSubentity = async <TUpdate, TGet>(
  req: Request,
  res: Response,
  method: (entity: TUpdate) => Promise<TGet>,
) => {
  await uiUpdate<TUpdate, TGet>(req, res, method);
};

export const uiGeneralMethod = async <ERequest, TResponse>(
  req: Request,
  res: Response,
  method: (request?: ERequest) => Promise<TResponse>,
) => {
  try {
    const result = await method(req.body);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiGeneralMethod. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiGeneralGetMethod = async <ERequest, TResponse>(
  req: Request,
  res: Response,
  method: (request?: ERequest) => Promise<TResponse>,
) => {
  try {
    const result = await method((req.query as unknown) as ERequest);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiGeneralMethod. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

// uiListSubentity
export const uiListSubentity = async <TForList>(
  req: Request,
  res: Response,
  method: (id: number) => Promise<TForList[]>,
) => {
  try {
    const result = await method(parseInt(req.params.id, 10));
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiListSubentity. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiListSubentityStr = async <TForList>(
  req: Request,
  res: Response,
  method: (id: string) => Promise<TForList[]>,
) => {
  try {
    const result = await method(req.params.id);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiListSubentity. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiListGeneralSubentity = list;

// export const uiListGeneralSubentity = async <TForList, TForListRequest>(
//   req: Request,
//   res: Response,
//   method: (request?: TForListRequest) => Promise<TForList[]>,
// ) => {
//   await uiList<TForList, TForListRequest>(req, res, method);
// };

export const byIdGeneralSubentity = async <T>(
  req: Request,
  res: Response,
  method: (id: number) => Promise<T>,
) => {
  try {
    const result = await method(parseInt(req.params.id, 10));
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared byIdGeneralSubentity. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const byIdGeneralSubentityStr = async <T>(
  req: Request,
  res: Response,
  method: (id: string) => Promise<T>,
) => {
  try {
    const result = await method(req.params.id);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared byIdGeneralSubentity. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiEntityMethod = async <ERequest, TResponse>(
  req: Request,
  res: Response,
  method: (id: number, request?: ERequest) => Promise<TResponse>,
) => {
  try {
    const result = await method(parseInt(req.params.id, 10), req.body);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiEntityMethod. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const uiEntityMethodStr = async <ERequest, TResponse>(
  req: Request,
  res: Response,
  method: (id: string, request?: ERequest) => Promise<TResponse>,
) => {
  try {
    const result = await method(req.params.id, req.body);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared uiEntityMethod. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const generalSubentityEntityMethod = async <ERequest, TResponse>(
  req: Request,
  res: Response,
  method: (id: string, request?: ERequest) => Promise<TResponse>,
) => {
  try {
    const result = await method(req.params.id, req.body);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared generalSubentityEntityMethod. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};

export const generalSubentityGeneralMethod = async <ERequest, TResponse>(
  req: Request,
  res: Response,
  method: (request?: ERequest) => Promise<TResponse>,
) => {
  try {
    const result = await method(req.body);
    res.send(sucess(result));
  } catch (error_) {
    log.error(`shared generalSubentityGeneralMethod. err: ${error_}`);
    res.send(error(error_));
    throw error_;
  }
};
