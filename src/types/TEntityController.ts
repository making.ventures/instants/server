import {Request, Response} from 'express';

export type TEntityController<TService> = Record<
  keyof TService,
  (req: Request, res: Response) => void
>;
