import {Router} from 'express';

export interface IBaseController {
  getRouter(): Router;
}
