/* eslint-disable promise/prefer-await-to-callbacks */
import {Express} from 'express';
import multer from 'multer';
import {v4 as uuidv4} from 'uuid';
import mkdirp from 'mkdirp';

// File Uploads

export const upload = async (
  app: Express,
  uploadsDir: string,
  urlPath: string,
  filestoreHost: string,
  middlewares: Array<(req: any, res: any, next: any) => Promise<void>>,
) => {
  mkdirp(uploadsDir);
  const storage = multer.diskStorage({
    destination(_: any, __: any, cb) {
      cb(null, uploadsDir);
    },
    filename(_: any, file, cb) {
      const uniq = uuidv4();
      mkdirp(`${uploadsDir}/${uniq}`);

      // cb(null, uuidv4() + path.extname(file.originalname));
      cb(null, `${uniq}/${file.originalname}`);
    },
  });
  const upload = multer({storage}).single('file');
  app.post(`${urlPath}/upload`, ...middlewares, (req: any, res: any) => {
    upload(req, res, (err: any) => {
      if (err instanceof multer.MulterError) {
        return res.status(500).json(err);
      } else if (err) {
        return res.status(500).json(err);
      }

      return res.status(200).send({
        data: {
          url: `http://${filestoreHost}/uploads/${req.file.filename}`,
        },
        status: 'success',
      });
    });
  });
};
