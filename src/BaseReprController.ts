import express, {Request, Response} from 'express';
import {BaseController} from './BaseController';
import {reprGet, reprList} from './methods';
import {IBaseEntity, IBaseUIReprService} from '@instants/core';

export class BaseReprController<
  Model extends IBaseEntity<number>,
  Repo extends IBaseUIReprService<Model>
> extends BaseController
  implements
    Record<keyof IBaseUIReprService, (req: Request, res: Response) => void> {
  protected service: Repo;

  public constructor(service: Repo) {
    super();
    this.service = service;
  }

  public getReprRouter() {
    const reprRouter = express.Router();
    reprRouter.get('/list', this.reprList.bind(this));
    reprRouter.get('/entity/:id', this.reprGet.bind(this));

    return reprRouter;
  }

  public getRouter() {
    const router = express.Router();
    const reprRouter = this.getReprRouter();
    router.use('/repr', reprRouter);

    const uiRouter = this.getUIRouter();
    router.use('/ui', uiRouter);

    return router;
  }

  public getUIRouter() {
    const router = express.Router();

    return router;
  }

  public async reprGet(req: Request, res: Response) {
    await reprGet(req, res, this.service.reprGet.bind(this.service));
  }

  public async reprList(req: Request, res: Response) {
    await reprList(req, res, this.service.reprList.bind(this.service));
  }
}
