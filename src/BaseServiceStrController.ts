import express, {Request, Response} from 'express';
import {BaseController} from './BaseController';
import {create, deleteMethodStr, list, update, getStr} from './methods';
import {BaseReprStrController} from './BaseReprStrController';
import {IBaseEntity, IListRequest, IBaseStrService, IBaseCRUDService} from '@instants/core';

export abstract class BaseServiceStrController<
  Model extends IBaseEntity<string>,
  Repo extends IBaseStrService<Model>
> extends BaseController
  implements
    Record<keyof IBaseCRUDService, (req: Request, res: Response) => void> {
  protected reprController: BaseReprStrController<Model, Repo>;

  protected service: Repo;

  public constructor(service: Repo) {
    super();
    this.service = service;
    this.reprController = new BaseReprStrController<Model, Repo>(this.service);
  }

  public async get(req: Request, res: Response) {
    await getStr<Model>(req, res, this.service.get.bind(this.service));
  }

  public async create(req: Request, res: Response) {
    await create<Model>(req, res, this.service.create.bind(this.service));
  }

  public async delete(req: Request, res: Response) {
    await deleteMethodStr(req, res, this.service.delete.bind(this.service));
  }

  public getCustomRouter() {
    const router = express.Router();

    return router;
  }

  public getMethodsRouter() {
    const router = express.Router();

    return router;
  }

  public getRouter() {
    const router = express.Router();

    const basicRouter = express.Router();
    basicRouter.get('/list', this.list.bind(this));
    basicRouter.get('/entity/:id', this.get.bind(this));
    basicRouter.post('/entity/', this.create.bind(this));
    basicRouter.put('/entity/:id', this.update.bind(this));
    basicRouter.delete('/entity/:id', this.delete.bind(this));
    router.use('/basic', basicRouter);

    const methodsRouter = this.getMethodsRouter();
    router.use('/methods', methodsRouter);

    const customRouter = this.getCustomRouter();
    router.use('/custom', customRouter);

    const uiRouter = this.getUIRouter();
    router.use('/ui', uiRouter);

    // Const uiRouter = this.getUIRouter();
    // Router.use("/ui", uiRouter);

    const reprRouter = this.getReprRouter();
    router.use('/repr', reprRouter);

    return router;
  }

  public getUIRouter() {
    const router = express.Router();

    return router;
  }

  // tslint:disable-next-line: member-ordering
  public getReprRouter() {
    const router = this.reprController.getReprRouter();

    return router;
  }

  public async list(req: Request, res: Response) {
    await list<Model, IListRequest>(
      req,
      res,
      this.service.list.bind(this.service),
    );
  }

  public async update(req: Request, res: Response) {
    await update<Model>(req, res, this.service.update.bind(this.service));
  }

  public async reprGet(req: Request, res: Response) {
    await this.reprController.reprGet(req, res);
  }

  public async reprList(req: Request, res: Response) {
    await this.reprController.reprList(req, res);
  }
}
